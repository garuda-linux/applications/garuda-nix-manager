#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class QAbstractButton;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_searchbar_textChanged(const QString &arg1);
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::MainWindow *ui;
    QString root_path;

    void populatePackageList();
    void errorMessage(QString message, bool critical);
    QStringList readManagedPackages();
    void writeManagedPackages();
};
#endif // MAINWINDOW_H
