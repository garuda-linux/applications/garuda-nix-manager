#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonArray>
#include <QMessageBox>
#include <QStringList>
#include <QJsonObject>
#include <QTemporaryFile>
#include <QTextStream>
#include <QProcess>
#include <QCoreApplication>
#include <QDir>
#include <QAbstractButton>

#define PACKAGE_LIST_PATH "/run/current-system/sw/share/garuda/package-list.json"
#define GARUDA_MANAGED "/etc/nixos/garuda-managed.json"
#define PKEXEC_TARGET ("/run/current-system/sw/lib/garuda-nix-manager/garuda-nix-manager-pkexec")

// Read .v2.user.extrapackages (which is an array of package names)
QStringList MainWindow::readManagedPackages() {
    QFile garuda_managed_file(GARUDA_MANAGED);
    if (!garuda_managed_file.open(QIODevice::ReadOnly)) {
        errorMessage("garuda-managed.json file could not be opened. Is garuda-nix-manager properly installed?", true);
        QApplication::exit(1);
        return QStringList();
    }
    QTextStream garuda_managed_text(&garuda_managed_file);
    QString json_string;
    json_string = garuda_managed_text.readAll();
    garuda_managed_file.close();
    QByteArray json_bytes = json_string.toLocal8Bit();

    auto json_doc = QJsonDocument::fromJson(json_bytes);
    // get .v2.user.extrapackages
    auto array = json_doc.object()["v2"].toObject()["user"].toObject()["extrapackages"].toArray();
    QStringList list;
    for (auto name : array)
    {
        list.append(name.toString());
    }
    return list;
}

void MainWindow::writeManagedPackages() {
    QJsonArray array;
    for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
        if (ui->tableWidget->item(i, 0)->checkState() == Qt::Checked) {
            array.append(ui->tableWidget->item(i, 1)->text());
        }
    }
    QJsonDocument doc;
    doc.setArray(array);

    QTemporaryFile file = QTemporaryFile(this);
    file.open();
    file.setAutoRemove(true);
    QTextStream stream(&file);
    stream << doc.toJson();
    stream.flush();

    QProcess process = QProcess();
    process.start("launch-terminal", QStringList() << QString("pkexec '") + PKEXEC_TARGET + "' apply '" + file.fileName() + "'");
    if (!process.waitForStarted())
    {
        errorMessage("Failed to apply changes. Is garuda-nix-manager properly installed?", false);
        errorMessage(process.errorString(), false);
        process.close();
        return;
    }
    process.waitForFinished();
    if (process.exitCode() != 0 || !file.exists()) {
        errorMessage("Failed to apply changes.", false);
    }
    process.close();
}

void MainWindow::populatePackageList() {
    QFile package_list_file(PACKAGE_LIST_PATH);
    if (!package_list_file.open(QIODevice::ReadOnly)) {
        errorMessage("Package list file could not be opened. Is garuda-nix-manager properly installed?", true);
        QApplication::exit(1);
        return;
    }
    QTextStream package_list_text(&package_list_file);
    QString json_string;
    json_string = package_list_text.readAll();
    package_list_file.close();
    QByteArray json_bytes = json_string.toLocal8Bit();

    auto json_doc = QJsonDocument::fromJson(json_bytes);
    auto array = json_doc.array();

    auto managed_packages = readManagedPackages();

    // Populate the table
    ui->tableWidget->setRowCount(0);
    for (auto name : array)
    {
        auto row = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow(row);
        auto checkbox = new QTableWidgetItem();
        checkbox->setCheckState(managed_packages.contains(name.toString()) ? Qt::Checked : Qt::Unchecked);
        ui->tableWidget->setItem(row, 0, checkbox);
        ui->tableWidget->setItem(row, 1, new QTableWidgetItem(name.toString()));
    }
}

void MainWindow::errorMessage(QString message, bool critical)
{
    QMessageBox::critical(this, critical ? "A critical error has occoured" : "An error has occoured", message);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    populatePackageList();
    QDir appdir = QDir(QCoreApplication::applicationDirPath());
    appdir.cdUp();
    root_path = appdir.absolutePath();
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Filter tableWidget, last_search is used to optimize the search based on only the visible rows
void MainWindow::on_searchbar_textChanged(const QString &arg1)
{
    static QString last_search;
    if (arg1.startsWith(last_search)) {
        for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
            if (!ui->tableWidget->isRowHidden(i) && !ui->tableWidget->item(i, 1)->text().contains(arg1))
                ui->tableWidget->hideRow(i);
        }
    } else {
        for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
            if (ui->tableWidget->item(i, 1)->text().contains(arg1)) {
                ui->tableWidget->showRow(i);
            } else {
                ui->tableWidget->hideRow(i);
            }
        }
    }
    last_search = arg1;
}



void MainWindow::on_buttonBox_clicked(QAbstractButton *button)
{
    switch (ui->buttonBox->standardButton(button)) {
    case ui->buttonBox->Discard:
        ui->tableWidget->clear();
        populatePackageList();
        on_searchbar_textChanged(ui->searchbar->text());
        break;
    case ui->buttonBox->Apply:
        writeManagedPackages();
        break;
    default:
        break;
    }
}
